      CREATE TABLE "department"
    (
        "department_id" BIGSERIAL PRIMARY KEY,
        "department_name" VARCHAR(100) NOT NULL,
        "created_by" VARCHAR(64) NOT NULL,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        "updated_by" VARCHAR(64),
        "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        "deleted_at" TIMESTAMP WITH TIME ZONE
    );

    CREATE TABLE "location"
    (
        "location_id" BIGSERIAL PRIMARY KEY,
        "location_name" VARCHAR(100) NOT NULL,
        "created_by" VARCHAR(64) NOT NULL,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        "updated_by" VARCHAR(64),
        "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        "deleted_at" TIMESTAMP WITH TIME ZONE
    );
    
    CREATE TABLE "position"
    (
        "position_id" BIGSERIAL PRIMARY KEY,
        "department_id" BIGINT,
        "position_name" VARCHAR(100) NOT NULL,
        "created_by" VARCHAR(64) NOT NULL,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        "updated_by" VARCHAR(64),
        "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        "deleted_at" TIMESTAMP WITH TIME ZONE
    );

    CREATE TABLE "employee"
    (
        "employee_id" BIGSERIAL PRIMARY KEY,
        "employee_code" VARCHAR(10) NOT NULL,
        "employee_name" VARCHAR(100) NOT NULL,
        "password" VARCHAR(100) NOT NULL,
        "department_id" BIGINT,
        "position_id" BIGINT,
        "superior" BIGINT NOT NULL,
        "created_by" VARCHAR(64) NOT NULL,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        "updated_by" VARCHAR(64),
        "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        "deleted_at" TIMESTAMP WITH TIME ZONE
    );
    ALTER TABLE "employee" ADD FOREIGN KEY ("department_id") REFERENCES "department" ("department_id");
    ALTER TABLE "employee" ADD FOREIGN KEY ("position_id") REFERENCES "position" ("position_id");

    CREATE TABLE "attendance"
    (
        "attendance_id" BIGSERIAL PRIMARY KEY,
        "employee_id" BIGINT,
        "location_id" BIGINT,
        "absent_in" TIMESTAMP WITH TIME ZONE,
        "absent_out" TIMESTAMP WITH TIME ZONE,
        "created_by" VARCHAR(64) NOT NULL,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
        "updated_by" VARCHAR(64),
        "updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
        "deleted_at" TIMESTAMP WITH TIME ZONE
    );
    ALTER TABLE "attendance" ADD FOREIGN KEY ("employee_id") REFERENCES "employee" ("employee_id");
    ALTER TABLE "attendance" ADD FOREIGN KEY ("location_id") REFERENCES "location" ("location_id");