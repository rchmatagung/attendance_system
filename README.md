# Attendance System

Attendance System atau sistem absensi ini merupakan sistem yang diperuntukkan karyawan untuk melakukan absensi masuk dan absensi keluar secara realtime ( **Tugas Internship** )

## API Reference

### Master Department

#### Create Department

```http
  POST /api/v1/department
```

| Parameter         | Type     | Description  |
| :---------------- | :------- | :----------- |
| `department_name` | `string` | **Required** |

#### Get All Department

```http
  GET /api/v1/department
```

#### Get Department By ID

```http
  GET /api/v1/department/getbyid
```

| Parameter | Type    | Description  |
| :-------- | :------ | :----------- |
| `id`      | `int64` | **Required** |

#### Get Department By Name

```http
  GET /api/v1/department/getbyname
```

| Parameter        | Type     | Description  |
| :--------------- | :------- | :----------- |
| `departmentName` | `string` | **Required** |

#### Update Department

```http
  PUT /api/v1/department
```

| Parameter         | Type     | Description  |
| :---------------- | :------- | :----------- |
| `department_id`   | `int64`  | **Required** |
| `department_name` | `string` | **Required** |

#### Delete Department

```http
  DELETE /api/v1/department
```

| Parameter      | Type    | Description  |
| :------------- | :------ | :----------- |
| `departmentId` | `int64` | **Required** |

### Master Location

#### Create Location

```http
  POST /api/v1/location
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `location_name` | `string` | **Required** |

#### Get All Location

```http
  GET /api/v1/location
```

#### Get Location By ID

```http
  GET /api/v1/location/getbyid
```

| Parameter | Type    | Description  |
| :-------- | :------ | :----------- |
| `id`      | `int64` | **Required** |

#### Get location By Name

```http
  GET /api/v1/location/getbyname
```

| Parameter      | Type     | Description  |
| :------------- | :------- | :----------- |
| `locationName` | `string` | **Required** |

#### Update Location

```http
  PUT /api/v1/location
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `location_id`   | `int64`  | **Required** |
| `location_name` | `string` | **Required** |

#### Delete Location

```http
  DELETE /api/v1/location
```

| Parameter    | Type    | Description  |
| :----------- | :------ | :----------- |
| `locationId` | `int64` | **Required** |

### Master Position

#### Create Position

```http
  POST /api/v1/positions
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `department_id` | `int64`  | **Required** |
| `position_name` | `string` | **Required** |

#### Get Position By ID

```http
  GET /api/v1/positions/getbyid
```

| Parameter    | Type    | Description  |
| :----------- | :------ | :----------- |
| `positionId` | `int64` | **Required** |

#### Get Position By Name

```http
  GET /api/v1/positions/getbyname
```

| Parameter      | Type     | Description  |
| :------------- | :------- | :----------- |
| `positionName` | `string` | **Required** |

#### Get Position By Department ID

```http
  GET /api/v1/positions
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `department_id` | `string` | **Required** |

#### Update Position

```http
  PUT /api/v1/positions
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `position_id`   | `int64`  | **Required** |
| `department_id` | `int64`  | **Required** |
| `position_name` | `string` | **Required** |

#### Delete Position

```http
  PUT /api/v1/positions
```

| Parameter    | Type    | Description  |
| :----------- | :------ | :----------- |
| `positionId` | `int64` | **Required** |

### Employee

#### Login Employee

```http
  POST /api/v1/employee/login
```

| Parameter       | Type     | Description      |
| :-------------- | :------- | :--------------- |
| `employee_name` | `string` | **Required**     |
| `password`      | `string` | **Required**     |
| `role_name`     | `string` | **Not Required** |

#### **Note**: if you want to log in as an admin, please fill in role_name with admin and if you want to log in as an employee, you can leave it blank or you can fill it with employee

#### Create Employee

```http
  POST /api/v1/employee
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `employee_name` | `string` | **Required** |
| `password`      | `string` | **Required** |
| `department_id` | `int64`  | **Required** |
| `position_id`   | `int64`  | **Required** |

#### Get All Employee

```http
  GET /api/v1/employee
```

#### Get Employee By ID

```http
  GET /api/v1/employee/getbyid
```

| Parameter    | Type    | Description  |
| :----------- | :------ | :----------- |
| `employeeId` | `int64` | **Required** |

#### Get Employee By Name

```http
  GET /api/v1/employee/getbyname
```

| Parameter      | Type     | Description  |
| :------------- | :------- | :----------- |
| `employeeName` | `string` | **Required** |

#### Update Employee

```http
  PUT /api/v1/employee
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `employee_name` | `string` | **Required** |
| `department_id` | `int64`  | **Required** |
| `position_id`   | `int64`  | **Required** |
| `password`      | `string` | **Required** |
| `employee_id`   | `int64`  | **Required** |

#### Delete Employee

```http
  PUT /api/v1/employee
```

| Parameter    | Type    | Description  |
| :----------- | :------ | :----------- |
| `employeeId` | `int64` | **Required** |

### Attendance

#### Create Absent In

```http
  POST /api/v1/absentin
```

| Parameter     | Type    | Description  |
| :------------ | :------ | :----------- |
| `employee_id` | `int64` | **Required** |
| `location_id` | `int64` | **Required** |

#### Create Absent Out

```http
  PUT /api/v1/absentout
```

| Parameter     | Type    | Description  |
| :------------ | :------ | :----------- |
| `employee_id` | `int64` | **Required** |
| `location_id` | `int64` | **Required** |

#### Get All Attendance

```http
  GET /api/v1/absent
```

#### Get Attendance By ID

```http
  GET /api/v1/absent/getbyid
```

| Parameter       | Type    | Description  |
| :-------------- | :------ | :----------- |
| `attendance_id` | `int64` | **Required** |

#### Get Attendance By Employee ID

```http
  GET /api/v1/absent/getbyemployeeid
```

| Parameter     | Type    | Description  |
| :------------ | :------ | :----------- |
| `employee_id` | `int64` | **Required** |

#### Delete Attendance

```http
  PUT /api/v1/absent
```

| Parameter       | Type    | Description  |
| :-------------- | :------ | :----------- |
| `attendance_id` | `int64` | **Required** |

## Postman

[Postman](https://orange-comet-845857.postman.co/workspace/CRM-Attendance-System~622f1ca3-7035-427e-9791-4344b6436ced/collection/17521270-292f08db-1f65-4d70-a42b-e5eedba75650?action=share&creator=17521270)
