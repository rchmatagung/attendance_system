package core

import (
	"attendancesystem/config"
	"attendancesystem/internal/wrapper/usecase"

	location "attendancesystem/internal/core/location/delivery"

	"github.com/sirupsen/logrus"
	department	"attendancesystem/internal/core/department/delivery"
	positions	"attendancesystem/internal/core/positions/delivery"
	employee	"attendancesystem/internal/core/employee/delivery"
	attendance	"attendancesystem/internal/core/attendance/delivery"
)

type CoreHandler struct {
	Location	location.LocationHandler
	Department	department.DepartmentHandler
	Positions	positions.PositionsHandler
	Employee	employee.EmployeeHandler
	Attendance	attendance.AttendanceHandler
}

func NewCoreHandler(uc usecase.Usecase, conf *config.Config, log *logrus.Logger) CoreHandler {
	return CoreHandler{
		Location:	location.NewLocationHandler(uc, conf, log),
		Department:	department.NewDepartmentHandler(uc, conf, log),
		Positions:	positions.NewPositionsHandler(uc, conf, log),
		Employee:	employee.NewEmployeeHandler(uc, conf, log),
		Attendance:	attendance.NewAttendanceHandler(uc, conf, log),
	}
}
