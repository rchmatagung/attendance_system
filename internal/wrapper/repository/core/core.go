package core

import (
	"attendancesystem/config"
	location "attendancesystem/internal/core/location/repository"
	"attendancesystem/pkg/infra/db"

	"github.com/sirupsen/logrus"
	department	"attendancesystem/internal/core/department/repository"
	positions	"attendancesystem/internal/core/positions/repository"
	employee	"attendancesystem/internal/core/employee/repository"
	attendance	"attendancesystem/internal/core/attendance/repository"
)

type CoreRepository struct {
	Location	location.Repository
	Department	department.Repository
	Positions	positions.Repository
	Employee	employee.Repository
	Attendance	attendance.Repository
}

func NewCoreRepository(conf *config.Config, dbList *db.DatabaseList, log *logrus.Logger) CoreRepository {
	return CoreRepository{
		Location:	location.NewLocationRepo(dbList),
		Department:	department.NewDepartmentRepo(dbList),
		Positions:	positions.NewPositionsRepo(dbList),
		Employee:	employee.NewEmployeeRepo(dbList),
		Attendance:	attendance.NewAttendanceRepo(dbList),
	}
}
