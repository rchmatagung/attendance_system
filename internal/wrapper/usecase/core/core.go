package core

import (
	"attendancesystem/config"
	"attendancesystem/internal/wrapper/repository"
	"attendancesystem/pkg/infra/db"

	location "attendancesystem/internal/core/location/usecase"

	department "attendancesystem/internal/core/department/usecase"
	employee "attendancesystem/internal/core/employee/usecase"
	positions "attendancesystem/internal/core/positions/usecase"

	"github.com/sirupsen/logrus"
	attendance	"attendancesystem/internal/core/attendance/usecase"
)

type CoreUsecase struct {
	Location	location.Usecase
	Department	department.Usecase
	Positions	positions.Usecase
	Employee	employee.Usecase
	Attendance	attendance.Usecase
}

func NewCoreUsecase(repo repository.Repository, conf *config.Config, dbList *db.DatabaseList, log *logrus.Logger) CoreUsecase {
	return CoreUsecase{
		Location:	location.NewLocationUsecase(repo, conf, dbList, log),
		Department:	department.NewDepartmentUsecase(repo, conf, dbList, log),
		Positions:	positions.NewPositionsUsecase(repo, conf, dbList, log),
		Employee:	employee.NewEmployeeUsecase(repo, conf, dbList, log),
		Attendance:	attendance.NewAttendanceUsecase(repo, conf, dbList, log),
	}
}