package repository

import (
	"attendancesystem/internal/core/positions/models"
	"attendancesystem/pkg/infra/db"
	"context"
)

type Repository interface {
	Insert(ctx context.Context, params ...interface{}) (int64, error)
	GetAllPosition(ctx context.Context, departmentId int) (*[]models.ListPosition,int, error)
	GetPositionById(ctx context.Context, id int64) (*models.GetPosition, error)
	GetPositionByName(ctx context.Context, name string) (*models.GetPosition, error)
	Update(ctx context.Context, params ...interface{}) (int64, error)
	Delete(ctx context.Context, id int64) (int64, error)
	GetPositionByNameAndDeptId(ctx context.Context, name string, deptId int) (*models.GetPosition, error)
}

type PositionsRepo struct {
	DBList *db.DatabaseList
}

func NewPositionsRepo(dbList *db.DatabaseList) PositionsRepo {
	return PositionsRepo{
		DBList: dbList,
	}
}

func (p PositionsRepo) Insert(ctx context.Context, params ...interface{}) (int64, error) {
	var response int64
	err := p.DBList.DatabaseApp.Raw(qInsertPosition+qReturnID, params...).Scan(&response).Error
	return response, err
}

func (p PositionsRepo) GetAllPosition(ctx context.Context, departmentId int) (*[]models.ListPosition,int, error) {
	var response []models.ListPosition
	var count int
	var Where = "Where d.department_id = ? AND p.deleted_at IS NULL"
	var err error
	err = p.DBList.DatabaseApp.Raw(qCount+Where, departmentId).Scan(&count).Error
	if err != nil {
		return nil, 0, err
	}
	err = p.DBList.DatabaseApp.Raw(qSelectPosition+Where, departmentId).Scan(&response).Error
	return &response, count, err
}

func (p PositionsRepo) GetPositionById(ctx context.Context, id int64) (*models.GetPosition, error) {
	var response models.GetPosition
	err := p.DBList.DatabaseApp.Raw(qSelectPosition+qWhere+qPositionId+qAnd+qNotDeleted, id).Scan(&response).Error
	return &response, err
}

func (p PositionsRepo) GetPositionByName(ctx context.Context, name string) (*models.GetPosition, error) {
	var response models.GetPosition
	err := p.DBList.DatabaseApp.Raw(qSelectPosition+qWhere+qPositionNameCaseInSensitive+qAnd+qNotDeleted, name).Scan(&response).Error
	return &response, err
}

func (p PositionsRepo) GetPositionByNameAndDeptId(ctx context.Context, name string, deptId int) (*models.GetPosition, error) {
	var response models.GetPosition
	err := p.DBList.DatabaseApp.Raw(qSelectPosition+qWhere+qPositionNameCaseInSensitive+qAnd+qDeptId+qAnd+qNotDeleted, name, deptId).Scan(&response).Error
	return &response, err
}

func (p PositionsRepo) Update(ctx context.Context, params ...interface{}) (int64, error) {
	var response int64
	err := p.DBList.DatabaseApp.Raw(qUpdatePosition+qReturnID, params...).Scan(&response).Error
	return response, err
}

func (p PositionsRepo) Delete(ctx context.Context, id int64) (int64, error) {
	var response int64
	err := p.DBList.DatabaseApp.Raw(qDeletePosition+qReturnID, id).Scan(&response).Error
	return response, err
}