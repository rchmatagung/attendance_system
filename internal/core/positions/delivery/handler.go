package delivery

import (
	"attendancesystem/config"
	"attendancesystem/internal/core/positions/models"
	"attendancesystem/internal/wrapper/usecase"
	"attendancesystem/pkg/exception"
	"context"
	"fmt"
	"strconv"

	cm "attendancesystem/pkg/constants/message"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type PositionsHandler struct {
	Usecase usecase.Usecase
	Conf    *config.Config
	Log     *logrus.Logger
}

func NewPositionsHandler(uc usecase.Usecase, conf *config.Config, logger *logrus.Logger) PositionsHandler {
	return PositionsHandler{
		Usecase: uc,
		Conf:    conf,
		Log:     logger,
	}
}

func (p PositionsHandler) Create(c *fiber.Ctx) error {
	init := exception.InitException(c, p.Conf, p.Log)

	dataReq := new(models.CreatePositionRequest)
	if err := c.BodyParser(dataReq); err != nil {
		return exception.CreateResponse_Log(init, fiber.StatusBadRequest, cm.ErrBodyParser, cm.ErrBodyParserInd, nil)
	}

	username := fmt.Sprintf("%v", c.Locals("employee_name"))

	respData, errData := p.Usecase.Core.Positions.Create(context.Background(), dataReq, username)
	if errData != nil {
		return exception.CreateResponse_Log(init, errData.Code, errData.Message, errData.MessageInd, nil)
	}

	return exception.CreateResponse_Log(init, fiber.StatusCreated, "Success create position", "Sukses membuat posisi", respData)
}

func (p PositionsHandler) GetAllPosition(c *fiber.Ctx) error {
	init := exception.InitException(c, p.Conf, p.Log)

	departmentId, err := strconv.Atoi(c.Query("department_id"))
	if err != nil {
		return exception.CreateResponse_Log(init, fiber.StatusBadRequest, cm.ErrValuePageInt("department_id"), cm.ErrValuePageIntInd("department_id"), nil)
	}
	

	respData, total, errData := p.Usecase.Core.Positions.GetAllPosition(context.Background(), departmentId)
	if errData != nil {
		return exception.CreateResponse_Log(init, errData.Code, errData.Message, errData.MessageInd, nil)
	}

	return exception.CreateResponse_Log_Page(init, fiber.StatusOK, "Success get all position", "Sukses mendapatkan semua posisi", respData, 1, 0, total)
}

func (p PositionsHandler) GetPositionById(c *fiber.Ctx) error {
	init := exception.InitException(c, p.Conf, p.Log)

	positionId, err := strconv.Atoi(c.Query("positionId"))
	if err != nil {
		return exception.CreateResponse_Log(init, fiber.StatusBadRequest, cm.ErrValuePageInt("positionId"), cm.ErrValuePageIntInd("positionId"), nil)
	}

	respData, errData := p.Usecase.Core.Positions.GetPositionById(context.Background(), int64(positionId))
	if errData != nil {
		return exception.CreateResponse_Log(init, errData.Code, errData.Message, errData.MessageInd, nil)
	}

	return exception.CreateResponse_Log(init, fiber.StatusOK, fmt.Sprintf("Success get Position with id %v", positionId), fmt.Sprintf("Sukses mendapatkan posisi dengan id %v", positionId), respData)
}

func (p PositionsHandler) GetPositionByName(c *fiber.Ctx) error {
	init := exception.InitException(c, p.Conf, p.Log)

	positionName := c.Query("positionName")

	respData, errData := p.Usecase.Core.Positions.GetPositionByName(context.Background(), positionName)
	if errData != nil {
		return exception.CreateResponse_Log(init, errData.Code, errData.Message, errData.MessageInd, nil)
	}

	return exception.CreateResponse_Log(init, fiber.StatusOK, fmt.Sprintf("Success get Position with name %v", positionName), fmt.Sprintf("Sukses mendapatkan posisi dengan nama %v", positionName), respData)
}

func (p PositionsHandler) Update(c *fiber.Ctx) error {
	init := exception.InitException(c, p.Conf, p.Log)

	dataReq := new(models.UpdatePositionRequest)
	err := c.BodyParser(dataReq)
	if err != nil {
		return exception.CreateResponse_Log(init, fiber.StatusBadRequest, cm.ErrBodyParser, cm.ErrBodyParserInd, nil)
	}

	username := fmt.Sprintf("%v", c.Locals("employee_name"))

	errData := p.Usecase.Core.Positions.Update(context.Background(), dataReq, username)
	if errData != nil {
		return exception.CreateResponse_Log(init, errData.Code, errData.Message, errData.MessageInd, nil)
	}

	return exception.CreateResponse_Log(init, fiber.StatusOK, "Success update position", "Sukses mengubah posisi", nil)
}

func (p PositionsHandler) Delete(c *fiber.Ctx) error {
	init := exception.InitException(c, p.Conf, p.Log)

	positionId, err := strconv.Atoi(c.Query("positionId"))
	if err != nil {
		return exception.CreateResponse_Log(init, fiber.StatusBadRequest, cm.ErrValuePageInt("positionId"), cm.ErrValuePageIntInd("positionId"), nil)
	}

	username := fmt.Sprintf("%v", c.Locals("employee_name"))

	errData := p.Usecase.Core.Positions.Delete(context.Background(), int64(positionId), username)
	if errData != nil {
		return exception.CreateResponse_Log(init, errData.Code, errData.Message, errData.MessageInd, nil)
	}
	return exception.CreateResponse_Log(init, fiber.StatusOK, fmt.Sprintf("Success delete position with id %v", positionId), fmt.Sprintf("Sukses menghapus posisi dengan id %v", positionId), nil)
}