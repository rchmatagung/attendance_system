package positions

import (
	"attendancesystem/internal/middleware"
	"attendancesystem/internal/wrapper/handler"

	"github.com/gofiber/fiber/v2"
)

func NewRoutes(api fiber.Router, handler handler.Handler) {
	api.Post("/positions", middleware.AdminAuthMiddleware(),handler.Core.Positions.Create)
	api.Get("/positions", middleware.AdminAuthMiddleware(),handler.Core.Positions.GetAllPosition)
	api.Get("/positions/getbyid", middleware.AdminAuthMiddleware(),handler.Core.Positions.GetPositionById)
	api.Get("/positions/getbyname", middleware.AdminAuthMiddleware(),handler.Core.Positions.GetPositionByName)
	api.Put("/positions", middleware.AdminAuthMiddleware(),handler.Core.Positions.Update)
	api.Delete("/positions", middleware.AdminAuthMiddleware(),handler.Core.Positions.Delete)
}